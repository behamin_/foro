<?php

Route::group(['middleware' => 'web', 'prefix' => 'helloword', 'namespace' => 'Modules\Helloword\Http\Controllers'], function()
{
    Route::get('/', 'HellowordController@index');
});
