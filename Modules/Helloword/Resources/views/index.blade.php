@extends('helloword::layouts.master')

@section('content')
    <h1>Hello World</h1>
    {{$layoutId}}
    <p>
        This view is loaded from module: {!! config('helloword.name') !!}
    </p>
@stop
